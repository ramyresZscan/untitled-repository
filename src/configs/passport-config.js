
const passport = require("passport");
const BearerStrategy = require("passport-http-bearer");

/**
 * strategyCallback
 * @param {string} token - Token vindo no campo do cabeçalho Authorization
 * @param {function} done - Função callback do Passport Strategy
 */
function strategyCallback (token, done) {
  return done(null, { user: token }, { scope: "all" });
}

/**
 * Get Strategy
 * @returns {object} Passport Strategy
 */
function getStrategy () {
  return new BearerStrategy(strategyCallback);
}

passport.use(getStrategy());
