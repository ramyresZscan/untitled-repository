const { DEFAULT_PORT } = require("./constant-utils");

/**
 * normalizePort
 * @param {number} port
 */
function normalizePort (port) {
  if (!port) return DEFAULT_PORT;
  if (isNaN(port)) return DEFAULT_PORT;
  return parseInt(port);
}

module.exports = {
  normalizePort
};
