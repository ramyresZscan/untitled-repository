const express = require("express");
const passport = require("passport");
const morgan = require("morgan");
const helmet = require("helmet");
const cors = require("cors");

require("dotenv").config();
require("./configs/passport-config");

const { normalizePort } = require("./utils/number-utils");

/**
 * Init App Function
 * @example
 * initApp();
 */
function initApp () {
  const app = express();

  app.use(express.json());
  app.use(morgan("combined"));
  app.use(helmet());
  app.use(cors());

  app.get("/", passport.authenticate("bearer", { session: false }), (req, res, next) => {
    res.status(200).json({ message: "Hello World" });
  });

  app.use((req, res, next) => {
    const err = new Error("Not Found");
    err.status = 404;
    return next(err);
  });

  app.use((err, req, res, next) => {
    return res
      .status(err.status || 500)
      .json(err);
  });

  const port = normalizePort(process.env.PORT);

  app.listen(port, () => {
    console.log(`Server started at ${port}...`);
  });
}

if (require.main === module) {
  initApp();
}

module.exports = initApp;
