const should = require('should');
const numberUtils = require('../src/utils/number-utils');

/** 
 * @test {Number Utils} 
 * */
describe('Number Utils', () => {
    context('Number', () => {
        /** 
        * @test {Number Utils#normalizePort} 
        * */
        it('normalizePort(3000) should be equal 3000', () => {
            numberUtils.normalizePort(3000).should.be.deepEqual(3000);
        });
    });
});