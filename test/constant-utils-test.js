const should = require('should');
const constant = require('../src/utils/constant-utils');

/** 
 * @test {Constant Utils} 
 * */
describe('Constants', () => {

    context('Default Constant', () => {
        /** 
         * @test {Constant Utils#DEFAULT_PORT} 
         * */
        it('Constant DEFAULT_PORT should be equal 3001', () => {
            constant.DEFAULT_PORT.should.be.equal(3001);
        });
    });

});