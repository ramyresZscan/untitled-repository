const { src, dest, series } = require('gulp');
const { exec } = require('pkg')
const tar = require('gulp-tar');
const gzip = require('gulp-gzip');
const eslint = require('gulp-eslint');

const packageJson = require('./package.json');

function clean(cb) {
    cb();
}

function build(cb) {
    return src('src/**/*.*')
        .pipe(eslint())
        .pipe(dest('dist/build'));
}

function compact() {
    return src('dist/build/**/*.*')
        .pipe(tar(`${packageJson.name}_v${packageJson.version}.tar`))
        .pipe(gzip())
        .pipe(dest('dist/compacted'));
}

function pkg() {
    return exec(['src/index.js', '--targets', 'node10-win-x64', '--output', `dist/bin/${packageJson.name}_v${packageJson.version}.exe`])
}

exports.build = build;
exports.compact = compact;
exports.pkg = pkg;
exports.default = series(clean, build, compact, pkg);